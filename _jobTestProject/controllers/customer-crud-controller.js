mainApp
    .controller('customerCRUDController', ['$scope','customerInfo','customerService','_','$window',
        function ($scope,customerInfo,customerService,_,$window) {
            customerService.customers.removeSelection();
            $scope.customerArgs=customerService.options.customerArgs;
            $scope.formArgs = customerService.options.formArgs;

            $scope.cancel= function(){
                $window.history.back();
            }
            if (!customerInfo || _.isEmpty(customerInfo)){
                $scope.clonedCustomer=customerService.customers.getEmptyCustomer();
                return;
            }
                var customer = customerService.customers.findWhere({email:customerInfo.email});
                if (!customer) return;
                if (!customer.isLoaded) customer.update(customerInfo);
                customer.isSelected=true;
                $scope.customer= customer;
                $scope.clonedCustomer= _.clone(customer);

        }]);
