mainApp
    .controller('customerListController', ['$scope','customerService','emailList',
        function ($scope,customerService,emailList) {
            customerService.customers.initial(emailList);
            $scope.customerList= customerService.customers;
            $scope.$watch(customerService.customers.getList,
                function(newVal,oldVal){
                $scope.customers=newVal;
            })
        }]);
