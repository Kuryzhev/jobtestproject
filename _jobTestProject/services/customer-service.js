mainApp
    .service('customerService', ['customerDataFactory', '_', '$state','$location',
        function (customerDataFactory, _, $state,$location) {
            var self = this;
            this.customers = function Customers(){
                var customers=[];
                this.findWhere = function(expObj){
                    var foundIndex=_.findIndex(customers,expObj);
                    return customers[foundIndex];
                };
                this.add= function(customer){
                    this.removeSelection();
                    if (!customer) {
                        $state.go('customers.add');
                        return;
                    }
                    if (this.findWhere({email:customer.email})) throw new Error('this email is already being used');
                    customerDataFactory.save({email:customer.email},customer).$promise.then(function(){
                        customer.isLoaded=true;
                        customers.push(customer);
                        customerDataFactory.save({email:'initial'}, _.pluck(customers,'email'));
                        customer.select();
                    });
                };
                this.removeSelection =function(){
                    var selected=this.findWhere({isSelected: true});
                    if (selected) selected.isSelected=false;
                }
                return {
                    findWhere:this.findWhere,
                    add:this.add,
                    removeSelection:this.removeSelection,
                    initial: function (emailList) {
                        _.each(emailList, function (email) {
                            customers.push(new customer(email));
                        });
                    },
                    remove: function(customer){
                        customerDataFactory.remove({email:customer.email}).$promise.then(function(){
                            customers= _.without(customers,customer);
                            customerDataFactory.save({email:'initial'}, _.pluck(self.customers.getList(),'email'));
                            $state.go('customers')
                        })
                    },
                    getEmptyCustomer: function(){
                        return new customer();
                    },
                    getList: function(){
                        return customers;
                    }
                }
            }();
            //can be upgraded
            this.options = function () {
                var customerArgs = ['name', 'email', 'telephone', 'address', 'street', 'city', 'state', 'zip'];
                this.customerArgs = customerArgs;
                this.formArgs = _.map(customerArgs,function(custValue){
                    var formName=custValue.charAt(0).toUpperCase() + custValue.slice(1);
                    return {
                     name:formName ,
                     requiredMessage: formName+' field is required'
                    };
                });
               return {
                   customerArgs: function(){
                       return this.customerArgs;
                   }(),
                   formArgs: function(){
                       return this.formArgs;
                   }()
               }
            }();

            function customer(initial) {
                if (initial instanceof Object) {
                    for (var v = 0; v < self.options.customerArgs.length; v++) {
                        var prop = self.options.customerArgs[v];
                        this[prop] = initial[prop];
                    }
                } else {
                    this.email = initial;
                }
            }

            customer.prototype = {
                select: function () {
                    if (this.isSelected) return;
                    $state.go('customers.edit', {email: this.email});
                },
                clone: function(){
                    return _.clone(this);
                },
                save: function(customerInfo){
                    if (self.customers.findWhere({email:customerInfo.email})) throw new Error('this email is already being used');
                    var this_customer=this;
                    customerDataFactory.save({email:this_customer.email},function(){
                        var res = _.clone(customerInfo);
                        if (res.isSelected) delete res.isSelected;
                        if (res.isLoaded) delete res.isLoaded;
                        return res;
                    }()).$promise.then(function(){
                            $location.path($location.$$url.replace(this_customer.email,customerInfo.email));
                            $location.replace();
                            this_customer.update(customerInfo);
                            customerDataFactory.save({email:'initial'}, _.pluck(self.customers.getList(),'email'));
                        });
                },
                update: function(customerInfo){
                    customer.call(this,customerInfo);
                    this.isLoaded=true;
                    return this;
                }
            };


        }]);
