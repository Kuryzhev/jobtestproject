angular.module('mainApp.dataLayer',['ngResource']);
angular.module('mainApp.dataLayer').factory('customerDataFactory', ['$http','$resource', function ($http,$resource) {
    return $resource('/api/customers/:email', {email:'@email'}, {
        'get':    {method:'GET'},
        'query':    {method:'GET', isArray: true},
        'save':   {method:'POST'},
        'remove': {method:'DELETE'} });
}]);