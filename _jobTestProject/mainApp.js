var mainApp=angular.module('mainApp', ['ui.router','mainApp.dataLayer']);

mainApp.factory("_", function($window) {
    var _ = $window._;
    delete($window._);
    return (_);
});

mainApp.config(['$stateProvider','$urlRouterProvider',
    function ($stateProvider,$urlRouterProvider) {
        $urlRouterProvider.when('','/customers');
        $stateProvider
            .state('customers', {
                url: '/customers',
                templateUrl: "views/customer-list-view.html",
                controller: "customerListController",
                resolve:{
                    emailList: ['customerDataFactory','customerService',function(customerDataFactory){
                        return customerDataFactory.query({email:'initial'}).$promise;
                    }]
                }
            })
            .state('customers.add',{
                url:'/add',
                views:{
                    'sub-view': {
                        controller: 'customerCRUDController',
                        templateUrl: "views/customer-form-view.html"
                    }
                },
                resolve:{customerInfo: function(){
                    return null}}
            })
            .state('customers.edit',{
                url:'/:email',
                views:{
                    'sub-view': {
                        controller: 'customerCRUDController',
                        templateUrl: "views/customer-form-view.html",
                    }
                },
                resolve:{
                    customerInfo: ['customerDataFactory','$stateParams','customerService',function(customerDataFactory,$stateParams,customerService){
                        var existingCustomer=customerService.customers.findWhere({email:$stateParams.email});
                        return  (existingCustomer && existingCustomer.isLoaded)? existingCustomer : customerDataFactory.get({email:$stateParams.email}).$promise;
                    }]
                }
            });
        }]);