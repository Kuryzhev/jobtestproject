var express  = require('express');
var bodyParser = require('body-parser');
var methodOverride = require('method-override');
var fs = require("fs");
var app = express();
var dataUrl='/data/';   //data location

app.use(bodyParser.urlencoded({'extended':'true'}));
app.use(bodyParser.json());
app.use(bodyParser.json({ type: 'application/vnd.api+json' }));
app.use(methodOverride());
app.use(express.static(__dirname));

app.get('/api/customers/:email', function(req, res) {

    fs.readFile( __dirname + dataUrl + req.params.email+".json", 'utf8', function (err, data) {
        res.end( data );
    });
});

app.post('/api/customers/:email', function(req, res) {
    fs.writeFile(__dirname + dataUrl + req.params.email+".json", JSON.stringify(req.body), function(error) {
        res.sendStatus( error ? 500 : 200);
    });
    if (req.body.email && fs.existsSync(__dirname + dataUrl + req.params.email+".json"))
        fs.rename(__dirname + dataUrl + req.params.email+".json", __dirname + dataUrl + req.body.email+".json");
});

app.delete('/api/customers/:email', function(req, res) {
    fs.unlink(__dirname + dataUrl + req.params.email+".json", function(error) {
        res.sendStatus( error ? 500 : 200);
    });
});

app.get('*', function(req, res) {
    if (req.url === '/favicon.ico') {
        res.writeHead(200, {'Content-Type': 'image/x-icon'} );
        res.end();
        return;
    }
    res.sendFile('./index.html');
});
app.listen(8080);
console.log("App listening on port 8080");
